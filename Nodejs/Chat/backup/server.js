var app = require('http').createServer(response);
var fs = require('fs');
var io = require('socket.io')(app);

app.listen(3000);
console.log("App running...");

function response(req, res) {

    console.log('getting http response');

    var file = "";

    if (req.url == "/") {
        file = __dirname + '/index.html';
    } else {
        file = __dirname + req.url;
    }

    fs.readFile(file, function(err, data) {
        if (err) {
            res.writeHead(404);
            return res.end('Page or file not found');
        }

        res.writeHead(200);
        res.end(data);
    });

}

io.on("connection", function(socket) {

    console.log('connecting');

    socket.on("send message", function(sent_msg, callback) {
        
        var msg = JSON.parse(sent_msg);
        msg.Time = new Date();
        console.log(`message: ${msg.Message}, from: ${msg.User}, at: ${msg.Time}`);
        io.emit("update messages", msg);

        if (callback) {
            callback();
        }  
    });
});
