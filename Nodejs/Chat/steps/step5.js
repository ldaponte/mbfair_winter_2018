
    fs.readFile(file, function(err, data) {
        if (err) {
            res.writeHead(404);
            return res.end('Page or file not found');
        }

        res.writeHead(200);
        res.end(data);
    });
