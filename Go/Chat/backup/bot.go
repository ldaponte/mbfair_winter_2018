package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
)

//Message the message
type Message struct {
	User    string
	Message string
	Time    string
}

//Client global client
var client *gosocketio.Client

func main() {

	var err error
	client, err = gosocketio.Dial(
		gosocketio.GetUrl("localhost", 3000, false),
		transport.GetDefaultWebsocketTransport())

	if err != nil {
		log.Fatal(err)
	}

	log.Println("waiting for messages")

	err = client.On("update messages", func(h *gosocketio.Channel, args Message) {
		log.Println(fmt.Sprintf("From: (%s), Message: (%s)", args.User, args.Message))

		if args.User != "Bot" {
			SendMessage(fmt.Sprintf("I heard (%s) from (%s)", args.Message, args.User))
		}
	})

	if err != nil {
		log.Fatal(err)
	}

	for {
		time.Sleep(1 * time.Microsecond)
	}
}

//SendMessage Send message to others
func SendMessage(msg string) {

	message := Message{"Bot", msg, time.Now().Format(time.RFC3339)}
	messageData, err := json.Marshal(message)

	err = client.Emit("send message", string(messageData))

	if err != nil {
		log.Fatal(err)
	}
}
